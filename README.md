# OpenML dataset: electrical-grid-stability

https://www.openml.org/d/43007

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The analysis is performed for different sets of input values using the methodology similar to that described in [Schafer, Benjamin, et al. 'Taming instabilities in power grid networks by decentralized control.' The European Physical Journal Special Topics 225.3 (2016): 569-582.]. Several input values are kept the same: averaging time: 2 s; coupling strength: 8 s^-2; damping: 0.1 s^-1.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43007) of an [OpenML dataset](https://www.openml.org/d/43007). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43007/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43007/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43007/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

